#!/usr/bin/python
#--------------------------------------
#       Presence Sensor
# Detect punch speed
#
# This script tests the sensor on GPIO14.
#
# Author : Korigan
# Date   : 19/02/2017
#
# http://hackbbs.org/
#
# run: python ./wingchun.py
# Branchement: Une antenne sur le port 14 de la raspberry
#--------------------------------------

# Import required libraries
import RPi.GPIO as GPIO
import time
import datetime

pin=14
cpt=0
# Tell GPIO library to use GPIO references
GPIO.setmode(GPIO.BCM)	 

def calibrateWC(RCpin):
  #GPIO.setup(RCpin , GPIO.IN)
  print("Calibrating...")
  totalL=0
  totalH=0
  tot=0
  print("Eloigne toi du detecteur...")
  cpt=1
  for i in range(1,20000):
    res=RCtime(RCpin)
    if(res>0):
		totalL += res
		cpt+=1
  totalL = totalL/cpt
  print("Approche toi du detecteur...")
  cpt=1
  for i in range(1,20000):
    res=RCtime(RCpin)
    if(res>0):
		totalH += res
		cpt+=1
  totalH = totalH/cpt

  print("Seuil de detection: %d %d" % (totalL,totalH))
  return (totalL+totalH)/2
   
def main():
  # Wrap main content in a try block so we can
  # catch the user pressing CTRL-C and run the
  # GPIO cleanup function. This will also prevent
  # the user seeing lots of unnecessary error
  # messages.

  try:
    # Loop until users quits with CTRL-C
    oldVal=0
    cptPunch=0
    thresold = calibrateWC(pin)
    cptSuite = 0
    measureLow = 0
    avg=0
    starttime=time.time()
    timeActu=time.time()
    print("START PUNCHING!!!")
    while timeActu-starttime<60 :
      timeActu=time.time()
      val=RCtime(pin)
      if (val==0 and oldVal==0):
        measureLow+=1
      elif val==0:
        measureLow=1
      if val!=0 and oldVal!=0:
        cptSuite+=1  
      if val!=0:
        cptSuite=1
      if val>0:
        avg=(val+avg)/2  

      if ( val>thresold and measureLow>1 ):
        cptPunch+=1
        print "level=%d,%d,%d Suite: %d Cpt=%d" \
			%(val,measureLow, avg, cptSuite,cptPunch)
      oldVal=val
      time.sleep(0.0295)

  except KeyboardInterrupt:
    # Reset GPIO settings
    GPIO.cleanup()  

def RCtime(RCpin):
  measure = 0
  # Set Switch GPIO as input
  GPIO.setup(RCpin , GPIO.IN)
  # GPIO.output(RCpin, GPIO.HIGH)
  while (GPIO.input(pin)==GPIO.HIGH):
    measure += 1
  return measure

if __name__=="__main__":
   main()
